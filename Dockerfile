FROM node:10.12

RUN \
  apt-get update && \
  apt-get install -y python-dev zip && \
  wget https://bootstrap.pypa.io/get-pip.py && \
  python get-pip.py && \
  pip install awscli

